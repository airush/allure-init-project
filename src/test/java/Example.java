import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Test;

public class Example {
    @Test
    @Description("Check report if Step exits")
    public void test1(){
        sayHello();
    }

    @Step("Step 1. Hello message.")
    public void sayHello(){
        System.out.println("Hello world");
    }
}
